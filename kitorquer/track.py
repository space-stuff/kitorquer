import math

from pcbnewTransition import pcbnew
from shapely.geometry import MultiLineString
from .util import to_angle, to_point, to_vector
from shapely.geometry import MultiLineString


def points_on_arc(
    center=pcbnew.VECTOR2I(0, 0), start=pcbnew.VECTOR2I(1, 0), angle=math.pi, count=18
):
    """Calculate positions of points on an arc defined by its center, a start
    point, an angle, and the count of segments

    The points are defined in such a way that if they were connected by
    straight line segments, each segment would tangent the virtual circle
    defined by center and start point exactly in the middle of the segment.
    Only the first and last segment differ from this because there the start
    or end point, respectively, are tangent to the circle.
    """
    center = to_vector(center)  # center vector
    start = to_vector(start)  # start vector
    delta = angle / count  # angle between points
    v = start - center  # vector from center to start
    points = [start.getWxPoint()]  # start point is on circle
    r = v.EuclideanNorm()  # arc radius is length of v
    s = int(r * math.tan(delta / 2))  # length of first/last segment
    w = v + v.Perpendicular().Resize(s)  # vector of first segment

    for i in range(count):
        # append points between start and end of arc
        points.append((center + w.Rotate(i * delta)).getWxPoint())

    points.append((center + v.Rotate(angle)).getWxPoint())  # append end point

    # TODO: how to deal with logging?
    # print('points_on_arc: center at ({: 10.6f},{: 10.6f})'.format(
    #     pcbnew.ToMM(center.x), pcbnew.ToMM(center.y)))
    # print('points_on_arc: start at ({: 10.6f},{: 10.6f})'.format(
    #     pcbnew.ToMM(start.x), pcbnew.ToMM(start.y)))
    # print('points_on_arc: δ = {: 10.6f}°'.format(math.degrees(delta)))
    # print('points_on_arc: v = ({: 10.6f},{: 10.6f})'.format(
    #     pcbnew.ToMM(v.x), pcbnew.ToMM(v.y)))
    # print('points_on_arc: r = {: 10.6f} mm'.format(
    #     pcbnew.ToMM(r)))
    # print('points_on_arc: s = {: 10.6f} mm'.format(
    #     pcbnew.ToMM(s)))
    # print('points_on_arc: w = ({: 10.6f},{: 10.6f})'.format(
    #     pcbnew.ToMM(w.x), pcbnew.ToMM(w.y)))

    # TODO: should output better be some kind of pcbnew.VECTOR2I array thingy?
    #       + What would be positive about this?
    return points


def add_track(
    board,
    start,
    end,
    width=0.15,
    layer=pcbnew.F_Cu,
    netcode=0,
    vector=pcbnew.wxPointMM(0, 0),
    angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Add track"""
    start = to_point(start)
    end = to_point(end)

    track = pcbnew.PCB_TRACK(board)

    track.SetStart(to_vector(start))
    track.SetEnd(to_vector(end))
    track.SetWidth(pcbnew.FromMM(width))
    track.SetNetCode(netcode)
    track.SetLayer(layer)
    track.Rotate(to_vector(rotation_center), to_angle(angle))
    track.Move(to_vector(vector))

    board.Add(track)

    if debug:
        s = "util.add_track:  from ({: 10.6f},{: 10.6f})" + " to ({: 10.6f},{: 10.6f})"
        print(
            s.format(
                pcbnew.ToMM(start.x),
                pcbnew.ToMM(start.y),
                pcbnew.ToMM(end.x),
                pcbnew.ToMM(end.y),
            )
        )

    return track


def add_tracks(
    board,
    points,
    width=pcbnew.FromMM(0.15),
    layer=pcbnew.F_Cu,
    netcode=0,
    vector=pcbnew.wxPointMM(0, 0),
    angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Add tracks"""
    if debug:
        print("util.add_tracks: add tracks")

    tracks = []

    for i in range(len(points) - 1):
        tracks.append(
            add_track(
                board,
                points[i],
                points[i + 1],
                width,
                layer,
                netcode,
                vector,
                angle,
                rotation_center,
                debug,
            )
        )

    return tracks


# TODO remove this in future versions or map it to add_pcb_arc() for the time being?
def add_arced_track(
    board,
    center=pcbnew.wxPointMM(0, 0),
    start=pcbnew.wxPointMM(1, 0),
    angle=180,
    elements=18,
    track_width=0.15,
    layer=pcbnew.F_Cu,
    netcode=0,
    offset=pcbnew.wxPointMM(0, 0),
    orientation=0,
    debug: bool = False,
):
    """Add arced track"""

    if debug:
        print("util.add_Arced_track: add arced track")

    points = points_on_arc(center, start, math.radians(angle), elements)
    arc = []
    for i in range(1, len(points)):
        arc.append(
            add_track(
                board,
                points[i - 1],
                points[i],
                pcbnew.FromMM(track_width),
                layer,
                netcode,
                offset,
                orientation,
                debug,
            )
        )

    return arc


def add_pcb_arc(board, start, center, end, width, layer, netcode, reverse=False):
    """
    Add an arced PCB geometry to the board by providing the center, start, and end
    point of the arc.
    """
    arc = pcbnew.PCB_ARC(board)

    start = to_point(start)
    center = to_point(center)
    end = to_point(end)

    # mid is the vector from center to start rotated about the center by half the angle between the two vectors
    center_vector = to_vector(center)
    start_vector = to_vector(start - center)
    end_vector = to_vector(end - center)

    angle = end_vector.Angle() - start_vector.Angle()
    if angle < 0:
        angle += 2.0 * math.pi

    mid_angle = angle / 2.0

    if not reverse:
        mid = to_point(center_vector + start_vector.Rotate(mid_angle))
    else:
        mid = to_point(center_vector - start_vector.Rotate(mid_angle))

    # print(start_vector.Angle())
    # print(center)
    # print(end_vector.Angle())

    arc.SetStart(start)
    arc.SetEnd(end)
    arc.SetMid(mid)
    arc.SetWidth(pcbnew.FromMM(width))
    arc.SetLayer(layer)
    arc.SetNetCode(netcode)

    board.Add(arc)

    return arc


def add_linestring(
    board,
    linestring,
    width=pcbnew.FromMM(0.15),
    layer=pcbnew.F_Cu,
    netcode: int = 0,
    vector=pcbnew.wxPointMM(0, 0),
    angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Add a line defined by a LineString to the board"""
    points = []
    if isinstance(linestring, MultiLineString):
        linestring = linestring.geoms[0]
    for j in range(len(linestring.xy[0])):
        points.append(pcbnew.wxPoint(linestring.xy[0][j], linestring.xy[1][j]))
    add_tracks(
        board, points, width, layer, netcode, vector, angle, rotation_center, debug
    )


def add_via(
    board,
    position=pcbnew.wxPointMM(0, 0),
    width=0.4,
    drill=0.2,
    top_layer=pcbnew.F_Cu,
    bottom_layer=pcbnew.B_Cu,
    via_type=pcbnew.VIATYPE_THROUGH,
    netcode=1,
    offset=pcbnew.wxPoint(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPoint(0, 0),
    debug=False,
):
    """Add via"""
    via = pcbnew.PCB_VIA(board)

    via.SetPosition(to_vector(position))
    via.SetViaType(via_type)
    via.SetTopLayer(top_layer)
    via.SetBottomLayer(bottom_layer)
    via.Rotate(to_vector(rotation_center), to_angle(rotation_angle))
    via.Move(to_vector(offset))
    via.SetNetCode(netcode)
    via.SetWidth(pcbnew.FromMM(width))
    via.SetDrill(pcbnew.FromMM(drill))

    board.Add(via)

    if debug:
        print(
            "util.add_via: add via at ({: 10.6f},{: 10.6f})".format(
                pcbnew.ToMM(position.x), pcbnew.ToMM(position.y)
            )
        )

    return via


def add_vias_on_arc(
    board,
    center=pcbnew.wxPointMM(0, 0),
    start=pcbnew.wxPointMM(10, 0),
    distance=0.55,
    count=3,
    width=pcbnew.FromMM(0.4),
    drill=pcbnew.FromMM(0.2),
    top_layer=pcbnew.F_Cu,
    bottom_layer=pcbnew.B_Cu,
    via_type=pcbnew.VIATYPE_THROUGH,
    netcode=1,
    offset=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Add vias on arc"""
    vector = to_vector(start - center)
    radius = pcbnew.ToMM(vector.EuclideanNorm())
    via_angle = 2 * math.asin(distance / (2 * radius))

    vias = []
    if debug:
        print("util.add_vias_on_arc: center = {}".format(center))
        print("util.add_vias_on_arc: start  = {}".format(start))
        print("util.add_vias_on_arc: vector = {}".format(vector.getWxPoint()))
        print("util.add_vias_on_arc: radius = {}".format(radius))
        print("util.add_vias_on_arc: distance = {}".format(distance))
        print("util.add_vias_on_arc: angle = {} deg".format(math.degrees(via_angle)))
        print("util.add_vias_on_arc: add {} vias".format(count))
    for i in range(count):
        position = center + vector.Rotate(i * via_angle).getWxPoint()
        vias.append(
            add_via(
                board,
                position,
                width,
                drill,
                top_layer,
                bottom_layer,
                via_type,
                netcode,
                offset,
                rotation_angle,
                rotation_center,
                debug,
            )
        )

    return vias
