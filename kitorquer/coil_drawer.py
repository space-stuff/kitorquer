from . import util
from .coil import EmbeddedAirCoil, shapely_arc
from .draw import draw_polygon
from .track import add_linestring, add_via
from kikit.substrate import closestIntersectionPoint
from pcbnewTransition import pcbnew
from shapely.geometry import LineString, MultiLineString, Polygon
from shapely.ops import linemerge


class CoilDrawer:
    _ANGLE = 90.0
    _coil = None
    _inbound_layers = [pcbnew.In2_Cu, pcbnew.In4_Cu, pcbnew.In6_Cu, pcbnew.In8_Cu]
    _outbound_layers = [pcbnew.In1_Cu, pcbnew.In3_Cu, pcbnew.In5_Cu, pcbnew.In7_Cu]

    def __init__(
        self,
        board: pcbnew.BOARD,
        coil: EmbeddedAirCoil,
        inbound_layers=None,
        outbound_layers=None,
    ):
        self._board = board
        self._coil = coil
        if inbound_layers:
            self._inbound_layers = inbound_layers
            self._outbound_layers = outbound_layers

    def draw(self, netcode: int = 1, debug=True):
        # draw fronts, tracks, and the final track
        if debug:
            draw_polygon(
                self._board, self._coil.milled_contour.exterior, pcbnew.User_1, 0.05
            )
            for interior in self._coil.milled_contour.interiors:
                draw_polygon(self._board, interior, pcbnew.User_1, 0.05)

            for front in self._coil.fronts:
                draw_polygon(self._board, front, pcbnew.User_2, 0.02)

            for track in self._coil.tracks:
                draw_polygon(self._board, track, pcbnew.User_3, 0.02)

            draw_polygon(self._board, self._coil.final_track, pcbnew.User_3, 0.02)

        # add outer vias
        for via in self._coil.outer_via_positions:
            add_via(
                self._board,
                util.to_point(via),
                self._coil.vias_size * 1e3,
                (self._coil.vias_size - 2.0 * self._coil.vias_anular_width) * 1e3,
                netcode=netcode,
            )

        # add inner vias
        for via in self._coil.inner_via_positions:
            add_via(
                self._board,
                util.to_point(via),
                self._coil.vias_size * 1e3,
                (self._coil.vias_size - 2.0 * self._coil.vias_anular_width) * 1e3,
                netcode=netcode,
            )

        self.draw_outbound_tracks(netcode=netcode, debug=False)
        self.draw_inbound_tracks(netcode=netcode, debug=False)

    def draw_outbound_tracks(self, netcode: int = 1, debug=False):
        """Draw the tracks from the inner vias to the outer vias

        Positive winding sense about the KiCAD z axis is assumed."""
        for i in range(self._coil._outer_via_count):
            # start at an inner via and end at an outer via
            start = self._coil.inner_via_positions[i]
            end = self._coil.outer_via_positions[i]

            linestrings = self.create_tracks(start, end, -self._ANGLE, debug)

            for ls in linestrings:
                add_linestring(
                    self._board,
                    ls,
                    self._coil.track_width * 1e3,
                    self._outbound_layers[i],
                    netcode,
                )

    def draw_inbound_tracks(self, netcode: int = 1, debug=False):
        """Draw the tracks from the outer vias to the inner vias

        Positive winding sense about the KiCAD z axis is assumed."""
        if debug:
            print(self._coil._inbound_layer_count)
        for i in range(self._coil.inbound_layer_count):
            # start at an inner via and end at an outer via
            start = self._coil.inner_via_positions[i + 1]
            end = self._coil.outer_via_positions[i]

            linestrings = self.create_tracks(start, end, self._ANGLE, debug)

            for ls in linestrings:
                add_linestring(
                    self._board,
                    ls,
                    self._coil.track_width * 1e3,
                    self._inbound_layers[i],
                    netcode,
                )

    def create_tracks(self, start, end, angle, debug=False) -> list:
        """Given a start and an end via create the tracks between the two

        TODO: Need to define the angle properly."""
        # from first inner via go in opposite annotation direction to final track.
        b = closestIntersectionPoint(
            start.coords[0],
            -self._coil.annotation.direction,
            self._coil.final_track,
            pcbnew.FromMM(5.0),
        )

        # from there go to intersection from first outer via in annotation direction with last track
        c = closestIntersectionPoint(
            end.coords[0],
            self._coil.annotation.direction,
            self._coil.final_track,
            pcbnew.FromMM(41.0),
        )

        arc_center = closestIntersectionPoint(
            end.coords[0],
            self._coil.annotation.direction,
            self._coil.tracks[0],
            pcbnew.FromMM(41.0),
        )

        # these first three points define the LineString from the inner via to the final track
        lead_in = LineString([start, b, c])

        # start collecting all the required LineStrings into a list
        linestrings = [lead_in]

        track: LineString
        for track in self._coil.tracks[::-1]:
            # construct arc around outer via with radius from via to intersection with a given angle
            arc = shapely_arc(arc_center, c, angle)

            # due to having shifted the center of the arc we need an extension to properly intersect in the following operations
            base_point = closestIntersectionPoint(
                arc.coords[-1],
                -self._coil.annotation.direction,
                self._coil._milled_contour.exterior,
                pcbnew.FromMM(41.0),
            )
            arc = linemerge([arc, LineString([arc.coords[-1], base_point])])
            if debug:
                draw_polygon(self._board, arc, pcbnew.User_4, line_width=0.02)

            # split arc by intersecting with the polygon created by the current track
            linestrings.append(arc.intersection(Polygon(track)))

            # create a polygon from the complete arc and a LineString from the end of the arc over the outer via to the start of the arc using Polygon(linemerge([<points>]))
            line = LineString([c, end, arc.coords[-1]])

            wedge = Polygon(linemerge([arc, line]))
            if not wedge.is_valid:
                # see https://shapely.readthedocs.io/en/stable/manual.html#object.buffer why buffering is necessary to avoid invalid polygons
                wedge = wedge.buffer(0)

            # substract the polygon from the current track and merge the resulting MultiLineString using linemerge(track.difference(<Polygon>))
            cut_track = track.difference(wedge)

            if isinstance(cut_track, MultiLineString):
                cut_track = linemerge(cut_track)

            linestrings.append(cut_track)

            # get intersection between outer via line and current track as starting point for next iteration
            c = closestIntersectionPoint(
                end.coords[0],
                self._coil.annotation.direction,
                track,
                pcbnew.FromMM(41.0),
            )

        linestrings.append(LineString([c, end]))

        return linestrings
