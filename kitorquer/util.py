#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pcbnewTransition import pcbnew
from enum import IntEnum
from shapely.geometry import Point


class Layer(IntEnum):
    F_Cu = 0
    In1_Cu = 1
    In2_Cu = 2
    In3_Cu = 3
    In4_Cu = 4
    In5_Cu = 5
    In6_Cu = 6
    In7_Cu = 7
    In8_Cu = 8
    In9_Cu = 9
    In10_Cu = 10
    In11_Cu = 11
    In12_Cu = 12
    In13_Cu = 13
    In14_Cu = 14
    In15_Cu = 15
    In16_Cu = 16
    In17_Cu = 17
    In18_Cu = 18
    In19_Cu = 19
    In20_Cu = 20
    In21_Cu = 21
    In22_Cu = 22
    In23_Cu = 23
    In24_Cu = 24
    In25_Cu = 25
    In26_Cu = 26
    In27_Cu = 27
    In28_Cu = 28
    In29_Cu = 29
    In30_Cu = 30
    B_Cu = 31
    B_Adhes = 32
    F_Adhes = 33
    B_Paste = 34
    F_Paste = 35
    B_SilkS = 36
    F_SilkS = 37
    B_Mask = 38
    F_Mask = 39
    Dwgs_User = 40
    Cmts_User = 41
    Eco1_User = 42
    Eco2_User = 43
    Edge_Cuts = 44
    Margin = 45
    B_CrtYd = 46
    F_CrtYd = 47
    B_Fab = 48
    F_Fab = 49


class SHAPE_T(IntEnum):
    SEGMENT = 0
    RECT = 1
    ARC = 2
    CIRCLE = 3
    POLYGON = 4
    CURVE = 5


class STROKE_T(IntEnum):
    S_SEGMENT = 0
    S_RECT = 1
    S_ARC = 2
    S_CIRCLE = 3
    S_POLYGON = 4
    S_CURVE = 5


class EDA_ANGLE_T(IntEnum):
    TENTHS_OF_A_DEGREE_T = 0
    DEGREES_T = 1
    RADIANS_T = 2


class EDA_TEXT_HJUSTIFY_T(IntEnum):
    GR_TEXT_HJUSTIFY_LEFT = -1
    GR_TEXT_HJUSTIFY_CENTER = 0
    GR_TEXT_HJUSTIFY_RIGHT = 1


class EDA_TEXT_VJUSTIFY_T(IntEnum):
    GR_TEXT_VJUSTIFY_TOP = -1
    GR_TEXT_VJUSTIFY_CENTER = 0
    GR_TEXT_VJUSTIFY_BOTTOM = 1


class VIA_TYPE(IntEnum):
    VIA_NOT_DEFINED = 0
    VIA_MICROVIA = 1
    VIA_BLIND_BURIED = 2
    VIA_THROUGH = 3


def from_meter(length):
    """Convert meter to KiCAD internal units"""
    return pcbnew.FromMM(length * 1e3)


def to_meter(length):
    """Convert KiCAD internal units to meter"""
    return pcbnew.ToMM(length) / 1e3


def to_point(instance):
    """Convert input to pcbnew wxPoint"""
    if isinstance(instance, pcbnew.wxPoint):
        point = instance

    elif isinstance(instance, pcbnew.VECTOR2I):
        point = pcbnew.wxPoint(instance.x, instance.y)

    elif isinstance(instance, pcbnew.PCB_VIA):
        point = instance.GetPosition()

    elif isinstance(instance, list):
        point = []

        for item in instance:
            point.append(to_point(item))

    elif isinstance(instance, tuple):
        if len(instance) == 2:
            point = pcbnew.wxPoint(instance[0], instance[1])

        else:
            raise TypeError("input tuple has more or less than two elements")
    elif isinstance(instance, Point):
        return pcbnew.wxPoint(instance.coords[0][0], instance.coords[0][1])

    else:
        raise TypeError("input parameter unknown for conversion to pcbnew.wxPoint")

    return point


def to_vector(instance):
    """Convert input to pcbnew VECTOR2I"""
    if isinstance(instance, pcbnew.VECTOR2I):
        # do nothing for vectors
        vector = instance

    elif isinstance(instance, pcbnew.wxPoint):
        # convert wxPoint directly to vector
        vector = pcbnew.VECTOR2I(instance)

    elif isinstance(instance, pcbnew.PCB_VIA):
        # convert via position
        vector = pcbnew.VECTOR2I(instance.GetPosition())

    elif isinstance(instance, list):
        # handle lists
        vector = []

        for item in instance:
            vector.append(to_vector(item))

    elif isinstance(instance, tuple):
        # handle tuples
        if len(tuple) == 2:
            vector = pcbnew.VECTOR2I(instance[0], instance[1])

        else:
            raise TypeError("input tuple has more or less than two elements")

    else:
        try:
            vector = pcbnew.VECTOR2I(instance)
        except:
            raise TypeError(
                f"input parameter type {type(instance)} unknown for conversion to pcbnew.VECTOR2I"
            )

    return vector


def to_angle(instance):
    """Convert input to `pcbnew.EDA_ANGLE`"""
    if isinstance(instance, pcbnew.EDA_ANGLE):
        return instance

    elif isinstance(instance, float):
        return pcbnew.EDA_ANGLE(instance, EDA_ANGLE_T.RADIANS_T)

    elif isinstance(instance, int):
        return to_angle(float(instance))

    else:
        raise TypeError(
            f"Input paramter type {type(instance)} not handled by to_angle()"
        )


def to_module_frame(module: pcbnew.FOOTPRINT, instance):
    """Return a copy of the instance transformed to the module coordinate frame"""
    offset = module.GetPosition()
    angle = module.GetOrientationRadians()

    if isinstance(instance, pcbnew.wxPoint):
        point = pcbnew.wxPoint(instance.x - offset.x, instance.y - offset.y)
        items = to_point(to_vector(point).Rotate(angle))
    elif isinstance(instance, list):
        items = []

        for item in instance:
            items.append(to_module_frame(module, item))
    else:
        raise TypeError("input parameter unknown for transformation to module frame")
    return items


def to_global_frame(module: pcbnew.FOOTPRINT, instance):
    """Return a copy of the instance transformed to the global coordinate frame"""
    offset = module.GetPosition()
    angle = module.GetOrientationRadians()

    if isinstance(instance, pcbnew.wxPoint):
        items = to_point(to_vector(instance).Rotate(-angle))
        items = pcbnew.wxPoint(items.x + offset.x, items.y + offset.y)
    elif isinstance(instance, list):
        items = []

        for item in instance:
            items.append(to_global_frame(module, item))
    else:
        raise TypeError("input parameter unknown for transformation to global frame")
    return items


def perpendicular_scale(
    p0=pcbnew.VECTOR2I(pcbnew.wxPointMM(0, 0)),
    p1=pcbnew.VECTOR2I(pcbnew.wxPointMM(0, 5)),
    length=pcbnew.FromMM(1),
):
    v = to_vector(p1) - to_vector(p0)
    w = v.Perpendicular().Resize(length)
    return p1 + to_point(w)


def mirror_about_y_axis(point, mirror=0):
    """
    Mirror the point about the horizontal axis given in mm
    """
    y = pcbnew.FromMM(mirror) - (point.y - pcbnew.FromMM(mirror))
    mirrored_point = pcbnew.wxPoint(point.x, y)

    return mirrored_point


def mirror_x_component(point, mirror):
    """
    Mirror the x component of a point w.r.t. the x component of the mirror point.
    """
    x = mirror.x - (point.x - mirror.x)
    mirrored_point = pcbnew.wxPoint(x, point.y)

    return mirrored_point


def mirror_y_component(point, mirror):
    """
    Mirror the y component of a point w.r.t. the y component of the mirror point.
    """
    y = mirror.y - (point.y - mirror.y)
    mirrored_point = pcbnew.wxPoint(point.x, y)

    return mirrored_point


def get_nearest_segment(target, points):
    target = to_vector(target)
    points = to_vector(points)

    distance = []
    for point in points:
        distance.append((point - target).EuclideanNorm())

    idx = sorted(range(len(distance)), key=lambda k: distance[k])[0:2]

    return to_point([points[idx[0]], points[idx[1]]]), min(idx)


def split_at_nearest_segment(target, points, how="y", keep_before=True):
    """Split segment at segment nearest to tearget point"""
    segment, end_index = get_nearest_segment(target, points)

    # point on split segment
    if how == "y":
        m = (segment[1].y - segment[0].y) / (segment[1].x - segment[0].x)
        b = -segment[1].x * m + segment[1].y
        split_point = pcbnew.wxPoint((target.y - b) / m, target.y)
    elif how == "x":
        raise NotImplementedError()
    elif how == "perpendicular":
        raise NotImplementedError()
    else:
        raise ValueError()

    # split segments at selected point
    out_points = []
    if keep_before:
        out_points.extend(points[0:end_index])
        out_points.append(split_point)
    else:
        out_points.append(split_point)
        out_points.extend(points[end_index:])

    return out_points


def project_orthogonally(a, b):
    a = to_vector(a)
    b = to_vector(b)

    s = (a.x * b.x + a.y * b.y) / (a.x * a.x + a.y * a.y) * a.EuclideanNorm()

    c = a.Resize(int(s))
    return c


def line_intersection(a, b):
    """Find the intersection p of lines a, b"""
    x1, y1 = a[0].x, a[0].y
    x2, y2 = a[1].x, a[1].y
    x3, y3 = b[0].x, b[0].y
    x4, y4 = b[1].x, b[1].y

    d12 = x1 * y2 - y1 * x2
    d34 = x3 * y4 - y3 * x4

    den = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)

    return pcbnew.wxPoint(
        (d12 * (x3 - x4) - (x1 - x2) * d34) / den,
        (d12 * (y3 - y4) - (y1 - y2) * d34) / den,
    )


def print_edge_information(edges):
    for e in edges:
        shape_type = e.GetShape()
        shape_name = SHAPE_T(shape_type).name
        shape_as_string = pcbnew.SHAPE_TYPE_asString(shape_type)
        shape_start_mm = pcbnew.ToMM(e.GetStart())
        shape_end_mm = pcbnew.ToMM(e.GetEnd())
        print(
            f"{shape_name:8} ({shape_type} - {shape_as_string:13}) from {format_point(shape_start_mm)} to {format_point(shape_end_mm)} on layer {e.GetLayer()}"
        )


def format_point(point):
    TUPLE_FORMAT = "({:6.2f}, {:6.2f})"
    return TUPLE_FORMAT.format(*point)


def get_through_vias(board: pcbnew.BOARD):
    """Return a list of through vias located on the board."""
    vias = [
        track
        for track in board.GetTracks()
        if (
            track.GetClass() == "PCB_VIA"
            and track.GetViaType() == pcbnew.VIATYPE_THROUGH
        )
    ]

    return vias
