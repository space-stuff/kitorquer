import pcbnew
from . import util
from shapely.geometry import LineString, Polygon


def draw_segment(
    board,
    start,
    end,
    layer=pcbnew.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw a straight line segment"""
    segment = pcbnew.PCB_SHAPE()
    segment.SetShape(pcbnew.S_SEGMENT)
    segment.SetLayer(layer)
    segment.SetWidth(pcbnew.FromMM(segment_width))
    segment.SetStart(util.to_vector(start))
    segment.SetEnd(util.to_vector(end))

    # rotate segment about a given point,
    # 1st argument of type wxPoint,
    # 2nd argument in tenth of a degree
    segment.Rotate(util.to_vector(rotation_center), util.to_angle(rotation_angle))

    # move segment by a given vector
    segment.Move(util.to_vector(center))

    if debug:
        print("util.draw_segment: add line segment")
    board.Add(segment)


def draw_segments(
    board,
    points,
    layer=pcbnew.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw segments"""
    for i in range(len(points) - 1):
        draw_segment(
            board,
            points[i],
            points[i + 1],
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )


def draw_arc(
    board,
    arc_center,
    arc_start,
    arc_angle,
    layer=pcbnew.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw an arc segment"""
    segment = pcbnew.PCB_SHAPE()
    segment.SetShape(pcbnew.S_ARC)
    segment.SetLayer(layer)
    segment.SetWidth(pcbnew.FromMM(segment_width))
    segment.SetCenter(arc_center)
    segment.SetStart(arc_start)
    segment.SetArcAngleAndEnd(arc_angle)

    # rotate segment about a given point,
    # 1st argument of type wxPoint,
    # 2nd argument in tenth of a degree
    segment.Rotate(rotation_center, rotation_angle)

    # move segment by a given vector
    segment.Move(center)

    if debug:
        print("util.draw_arc: add arc segment")
    board.Add(segment)


def draw_circle(
    board,
    center=pcbnew.wxPointMM(0, 0),
    radius=10.0,
    layer=pcbnew.Edge_Cuts,
    segment_width=0.1,
    offset=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw a circle line segment"""
    segment = pcbnew.PCB_SHAPE()
    segment.SetShape(pcbnew.S_CIRCLE)
    segment.SetLayer(layer)
    segment.SetWidth(pcbnew.FromMM(segment_width))
    segment.SetCenter(util.to_point(center))
    segment.SetStart(util.to_point(center) + pcbnew.wxPointMM(radius, 0))

    # rotate segment about a given point,
    # 1st argument of type wxPoint,
    # 2nd argument in tenth of a degree
    segment.Rotate(rotation_center, rotation_angle)

    # move segment by a given vector
    segment.Move(offset)

    if debug:
        print("util.draw_circle: add circle segment")
    board.Add(segment)


def draw_stadium(
    board,
    length,
    width,
    layer=pcbnew.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    offset=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw a stadium shape"""
    # TODO: adapt this similar to draw_rounded_rectangle

    if debug:
        print("util.draw_stadium: draw stadium")

    length_short = length - width

    p = []

    p.append(pcbnew.wxPointMM(length_short / 2, width / 2) + offset)
    p.append(pcbnew.wxPointMM(-length_short / 2, width / 2) + offset)
    p.append(pcbnew.wxPointMM(-length_short / 2, -width / 2) + offset)
    p.append(pcbnew.wxPointMM(length_short / 2, -width / 2) + offset)

    c = []

    c.append(pcbnew.wxPointMM(length_short / 2, 0) + offset)
    c.append(pcbnew.wxPointMM(-length_short / 2, 0) + offset)

    for i in range(0, len(p), 2):
        draw_segment(
            board,
            p[i],
            p[i + 1],
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )

    for i in range(0, len(c)):
        draw_arc(
            board,
            c[i],
            p[2 * i - 1],
            1800,
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )


def draw_rounded_rectangle(
    board,
    length,
    width,
    radius,
    layer=pcbnew.Edge_Cuts,
    segment_width=0.1,
    center=pcbnew.wxPointMM(0, 0),
    rotation_angle=0,
    rotation_center=pcbnew.wxPointMM(0, 0),
    offset=pcbnew.wxPointMM(0, 0),
    debug: bool = False,
):
    """Draw a rounded rectangle shape"""

    if debug:
        print("util.draw_rounded_rectangle: draw rounded rectangle")

    length_short = length - 2 * radius
    width_short = width - 2 * radius

    p = []

    p.append(pcbnew.wxPointMM(length_short / 2, width / 2) + offset)
    p.append(pcbnew.wxPointMM(-length_short / 2, width / 2) + offset)
    p.append(pcbnew.wxPointMM(-length / 2, width_short / 2) + offset)
    p.append(pcbnew.wxPointMM(-length / 2, -width_short / 2) + offset)
    p.append(pcbnew.wxPointMM(-length_short / 2, -width / 2) + offset)
    p.append(pcbnew.wxPointMM(length_short / 2, -width / 2) + offset)
    p.append(pcbnew.wxPointMM(length / 2, -width_short / 2) + offset)
    p.append(pcbnew.wxPointMM(length / 2, width_short / 2) + offset)

    c = []

    c.append(pcbnew.wxPointMM(length_short / 2, width_short / 2) + offset)
    c.append(pcbnew.wxPointMM(-length_short / 2, width_short / 2) + offset)
    c.append(pcbnew.wxPointMM(-length_short / 2, -width_short / 2) + offset)
    c.append(pcbnew.wxPointMM(length_short / 2, -width_short / 2) + offset)

    for i in range(0, len(p), 2):
        draw_segment(
            board,
            p[i],
            p[i + 1],
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )

    for i in range(0, len(c)):
        draw_arc(
            board,
            c[i],
            p[2 * i - 1],
            900,
            layer,
            segment_width,
            center,
            rotation_angle,
            rotation_center,
            debug,
        )


def draw_polygon(
    board: pcbnew.BOARD, polygon: Polygon, layer=pcbnew.F_SilkS, line_width=0.1
):
    points = []
    for j in range(len(polygon.xy[0])):
        points.append(pcbnew.wxPoint(polygon.xy[0][j], polygon.xy[1][j]))
    draw_segments(board, points, layer, line_width)


def draw_linestring(
    board: pcbnew.BOARD, linestring: LineString, layer=pcbnew.F_SilkS, line_width=0.1
):
    points = []
    for j in range(len(linestring.xy[0])):
        points.append(pcbnew.wxPoint(linestring.xy[0][j], linestring.xy[1][j]))
    draw_segments(board, points, layer, line_width)
