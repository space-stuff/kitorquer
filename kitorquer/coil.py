import os
import numpy as np

from itertools import chain
from kikit.annotations import AnnotationReader, TabAnnotation
from kikit.common import makePerpendicular, normalize
from kikit.substrate import closestIntersectionPoint, extractRings, Substrate, toShapely
from pcbnewTransition import pcbnew
from shapely.affinity import rotate
from shapely.errors import TopologicalError
from shapely.geometry import LineString, MultiPolygon, Point, Polygon
from .util import from_meter, get_through_vias, to_meter, to_vector, SHAPE_T

# copper density in kg/m^3
COPPER_DENSITY = 8920

# copper specific resistance at 20°C
SPECIFIC_RESISTANCE_20 = 16.78e-9

# copper temperature coefficient of specific resistance at 20°C
TEMPERATURE_COEFFICIENT_20 = 3.9e-3

REFERENCE_TEMPERATURE = 20.0


def specific_resistance(temperature):
    """Calculates the specific resistance of copper given temperature, based on the specific resistance and temperature coefficient of copper at 20°C"""
    return SPECIFIC_RESISTANCE_20 * (
        1 + TEMPERATURE_COEFFICIENT_20 * (temperature - REFERENCE_TEMPERATURE)
    )


def cross_sectional_area(track_width, track_thickness):
    """Calculates cross sectional area of copper track from the width and thickness of the copper track"""
    return track_width * track_thickness


def mass(length, cross_sectional_area):
    """Calculate copper mass of a coil from total conductor length and conductor cross-sectional area"""
    mass = COPPER_DENSITY * length * cross_sectional_area
    return mass


def current(voltage, resistance):
    """Calculate current through coil from the voltage across the coil and the total resistance of the copper track"""
    return voltage / resistance


def power(voltage, current):
    """Calculate power consumption of coil from voltage across coil and current through coil"""
    return voltage * current


def resistance(temperature, track_length, cross_sectional_area):
    """Calculates total resistance of coil from temperature, track length, and cross sectional area of the copper track"""
    return specific_resistance(temperature) * track_length / cross_sectional_area


def dipole_power_ratio(magnetic_dipole, power):
    """Calculates the magnetic dipole to power ratio of the coil"""
    return magnetic_dipole / power


def voltage(current, resistance):
    """Calculate the voltage across a coil from the fixed input current and resistance of the coil"""
    return current * resistance


def clear_multi_polygon(geom) -> Polygon:
    """Remove smaller not connected islands from a MultiPolygon object"""
    if isinstance(geom, MultiPolygon):
        area = []
        for t in geom.geoms:
            area.append(t.area)
        j = np.argmax(area)
        geom = geom.geoms[j]
    return geom


def separate_expendables(exterior, interiors, threshold):
    """Separate interiors into expendable and non-expendable geoms

    Separate interior linear rings into expendable and non-expendable linear rings depending on a distance threshold with
    respect to the exterior linear ring."""
    expendables = [i for i in interiors if i.distance(exterior) < threshold]
    interiors[:] = [i for i in interiors if i.distance(exterior) >= threshold]

    return expendables, interiors


def pad_has_hole(pad: pcbnew.PAD):
    """Return True if the pad has a drill size larger than zero."""
    try:
        foo: pcbnew.VECTOR2I
        foo = pad.GetDrillSize()
        if pcbnew.wxSize(0.0, 0.0) == foo.getWxSize():
            return False
        else:
            return True
    except:
        return False


def collect_footprint_holes(board: pcbnew.BOARD):
    """Return a list of pcbnew.PCB_SHAPE objects representing the drills of footprint pads on the board

    All PCB_SHAPE objects are of shape pcbnew.S_CIRCLE and have their start position at the pad center position. The end
    position is calculated from the hole diameter and set accordingly.

    The output of this function is inteded to be used together with the kikit.collectEdges method to create a kikit.Substrate
    object:

    .. code-block:: python

        edges = list(
            chain(collectEdges(board, Layer.Edge_Cuts), collect_footprint_holes(board))
        )

        substrate = Substrate(edges)

    """
    pads_with_holes = [
        pad for fp in board.GetFootprints() for pad in fp.Pads() if pad_has_hole(pad)
    ]
    holes = []
    pad: pcbnew.PAD
    for pad in pads_with_holes:
        circle = pcbnew.PCB_SHAPE()

        pad_position: pcbnew.VECTOR2I
        pad_position = pad.GetPosition()

        circle.SetShape(pcbnew.S_CIRCLE)
        circle.SetLayer(pcbnew.Edge_Cuts)
        circle.SetWidth(pcbnew.FromMM(0.1))
        circle.SetStart(pad_position)
        circle.SetEnd(
            to_vector(
                pad_position.getWxPoint() + pcbnew.wxPoint(pad.GetSizeX() / 2.0, 0.0)
            )
        )

        holes.append(circle)

    return holes


def collect_keepout_zones(board: pcbnew.BOARD):
    """Return a list of keepout zones where copper should not be placed.

    Note: this doesn't consider the layers configured in the keepout zone since
    (for now) we use the same substrate for all layers.
    """

    zones = []
    zone: pcbnew.ZONE
    for zone in board.Zones():
        if zone.GetIsRuleArea() and zone.GetDoNotAllowTracks():
            shape = pcbnew.PCB_SHAPE()
            shape.SetPolyShape(zone.Outline())
            shape.SetShape(SHAPE_T.POLYGON)

            # TODO: not sure about this.
            # seems to work for rectangles, may not work
            # for more complicated polygons
            shape.SetStart(zone.GetPosition())
            shape.SetEnd(zone.GetPosition())

            zones.append(shape)

    return zones


def collect_vias(board: pcbnew.BOARD):
    """Return a list of PCB_SHAPEs each via found in the board."""
    vias = get_through_vias(board)
    holes = []

    via: pcbnew.PCB_VIA
    for via in vias:
        start = via.GetPosition()
        end = start + to_vector(pcbnew.wxPoint(via.GetWidth() / 2.0, 0.0))

        circle = pcbnew.PCB_SHAPE()
        circle.SetShape(pcbnew.S_CIRCLE)
        circle.SetLayer(pcbnew.Edge_Cuts)
        circle.SetWidth(pcbnew.FromMM(0.1))
        circle.SetStart(start)
        circle.SetEnd(end)

        holes.append(circle)

    return holes


def extract_keepout_polygons(board: pcbnew.BOARD) -> Polygon | MultiPolygon:
    """Return a Polygon or MultiPolygon of keepout zones where copper should not be placed.

    Note: this doesn't consider the layers configured in the keepout zone since (for now) we use the same substrate
    for all layers.
    """

    zones = []
    zone: pcbnew.ZONE
    for zone in board.Zones():
        if zone.GetIsRuleArea() and zone.GetDoNotAllowTracks():
            shape = pcbnew.PCB_SHAPE()
            shape.SetPolyShape(zone.Outline())
            shape.SetShape(SHAPE_T.POLYGON)

            # TODO: not sure about this.
            # seems to work for rectangles, may not work
            # for more complicated polygons
            shape.SetStart(zone.GetPosition())
            shape.SetEnd(zone.GetPosition())

            zones.append(shape)

    polygons = [toShapely(ring, zones) for ring in extractRings(zones)]

    return polygons


def extract_substrate(board: pcbnew.BOARD = None, filename: str = None):
    """Collect edges and vias as well as footprint edges and pad drills into a kikit Substrate object.

    If neither a board nor a filename is passed as an input, this function assumes that is is run from within the KiCAD
    scripting environment and tries to get the board using the 'pcbnew.GetBoard()' constructor. If a valid Board instance is
    passed as an input, that board is used. If only a filename is passed, this function tries to load the board from that file
    using the 'pcbnew.LoadBoard(filename)' method.

    In addition to a Substrate object as known from kikit, this function returns a substrate that also contains the pad holes
    of footprints present on the board."""
    if not board:
        if not filename:
            board = pcbnew.GetBoard()
        else:
            board = pcbnew.LoadBoard(filename)

    substrate = Substrate(
        list(
            chain(
                collect_edge_cuts(board),
                collect_footprint_holes(board),
                collect_vias(board),
            )
        )
    )

    keepouts = extract_keepout_polygons(board)
    for keepout in keepouts:
        substrate.cut(keepout)

    return substrate


def shapely_arc(center, start, angle: float) -> LineString:
    """Define an arc by coordinates of its center and a start point together with an angle"""
    center = Point(center)
    start = Point(start)
    step = 360.0 / 128.0
    steps = int(abs(np.ceil(angle / step)))
    points = []
    for alpha in np.linspace(0.0, angle, steps):
        points.append(rotate(start, alpha, center))
    return LineString(points)


def collect_edge_cuts(board: pcbnew.BOARD):
    """Return a list of PCB_SHAPE objects located on the Edge.Cuts layer."""
    drawings = chain(
        board.GetDrawings(),
        *[footprint.GraphicalItems() for footprint in board.GetFootprints()],
    )
    edges = [edge for edge in drawings if is_board_edge(edge)]

    return edges


def is_board_edge(edge):
    """Return True if edge is a PCB_SHAPE object located on the Edge.Cuts layer."""
    return isinstance(edge, pcbnew.PCB_SHAPE) and edge.GetLayerName() == "Edge.Cuts"


class Coil:
    _conductor_length = None
    _cross_sectional_area = None
    _current = None
    _dipole_power_ratio = None
    _enclosed_area = None
    _magnetic_dipole = None
    _power = None
    _resistance = None
    _temperature = None
    _voltage = None

    _explicit_update = False

    def _calculate_conductor_length(self):
        NotImplementedError("_calculate_conductor_length() not implemented")

    def _calculate_cross_sectional_area(self):
        NotImplementedError("_calculate_cross_sectional_area() not implemented")

    def _calculate_current(self):
        self._current = self._voltage / self._resistance

    def _calculate_dipole_power_ratio(self):
        self._dipole_power_ratio = self._magnetic_dipole / self.power

    def _calculate_enclosed_area(self):
        NotImplementedError("_calculate_enclosed_area() not implemented")

    def _calculate_magnetic_dipole(self):
        self._magnetic_dipole = self._enclosed_area * self._current

    def _calculate_power(self):
        self._power = self._current * self._voltage

    def _calculate_resistance(self):
        self._resistance = resistance(
            self._temperature, self._conductor_length, self._cross_sectional_area
        )

    def _calculate_parameters(self):
        self._calculate_enclosed_area()
        self._calculate_conductor_length()
        self._calculate_cross_sectional_area()
        self._calculate_resistance()
        self._calculate_current()
        self._calculate_magnetic_dipole()
        self._calculate_power()
        self._calculate_dipole_power_ratio()

    def update(self):
        self._calculate_parameters()

    @property
    def conductor_length(self):
        return self._conductor_length

    @property
    def cross_sectional_area(self):
        return self._cross_sectional_area

    @property
    def current(self):
        return self._current

    @property
    def enclosed_area(self):
        return self._enclosed_area

    @property
    def magnetic_dipole(self):
        return self._magnetic_dipole

    @property
    def power(self):
        return self._power

    @property
    def properties(self):
        properties_dictionary = {
            "conductor_length": self._conductor_length,
            "cross_sectional_area": self._cross_sectional_area,
            "current": self._current,
            "dipole_power_ratio": self._dipole_power_ratio,
            "enclosed_area": self._enclosed_area,
            "magnetic_dipole": self._magnetic_dipole,
            "power": self._power,
            "resistance": self._resistance,
            "temperature": self._temperature,
            "voltage": self._voltage,
        }
        return properties_dictionary

    @property
    def resistance(self):
        return self._resistance

    @property
    def temperature(self):
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        self._temperature = value
        if not self._explicit_update:
            self._calculate_parameters()

    @property
    def voltage(self):
        return self._voltage

    @voltage.setter
    def voltage(self, value):
        self._voltage = value
        if not self._explicit_update:
            self._calculate_parameters()


class AirCoil(Coil):
    pass


class EmbeddedAirCoil(AirCoil):
    """A coil embedded in a printed circuit board"""

    _EXTENSION = pcbnew.FromMM(30.0)
    _SIMPLIFY = 20000

    _milled_contour: Polygon = None
    _fronts = None
    _tracks = None
    _lengths = None
    _areas = None
    _interiors = None

    _winding_width = None

    _annotation = None

    def __init__(
        self,
        board: pcbnew.BOARD,
        winding_count: int = 5,
        track_width: float = 400e-6,
        track_separation: float = 100e-6,
        layer_count: int = 7,
        copper_thickness: float = 35e-6,
        temperature: float = 20.0,
        voltage: float = 3.3,
        vias_size: float = None,
        vias_annular_width: float = None,
        explicit_update=False,
    ):
        # self._board = board
        self._extract_substrate(board)
        self._winding_count = winding_count
        self._track_width = track_width
        self._track_separation = track_separation
        self._set_layer_count(layer_count)
        self._copper_thickness = copper_thickness
        self._temperature = temperature
        self._voltage = voltage
        self._explicit_update = explicit_update
        self._min_track_separation = None

        _settings: pcbnew.BOARD_DESIGN_SETTINGS = board.GetDesignSettings()
        self._minimum_vias_size = to_meter(_settings.m_ViasMinSize)
        self._minimum_vias_annular_width = to_meter(_settings.m_ViasMinAnnularWidth)

        if not vias_size:
            if self._minimum_vias_size > self._track_width:
                self._vias_size = self._minimum_vias_size
            else:
                self._vias_size = self._track_width
        else:
            if vias_size < self._minimum_vias_size:
                # TODO: Issue a warning!
                self._vias_size = self._minimum_vias_size
            else:
                self._vias_size = vias_size
        self._vias_inset = max(self._vias_size, self._track_width) / 2.0
        self._vias_separation = (
            max(self._vias_size, self._track_width) + self._track_separation
        )

        if not vias_annular_width:
            self._vias_annular_width = self._minimum_vias_annular_width
        else:
            if vias_annular_width < self._minimum_vias_annular_width:
                # TODO: Issue a warning!
                self._vias_annular_width = self._minimum_vias_annular_width
            else:
                self._vias_annular_width = vias_annular_width

        self._edge_clearance = to_meter(_settings.m_CopperEdgeClearance)
        self._smallest_gap = (
            track_width + 2.0 * self._edge_clearance
        )  # TODO: This might not be correct!
        self._jump_back = self._smallest_gap - (track_width + track_separation)

        self._extract_annotation(board)
        self._create_milled_contour()

        self._create_track()
        self._calculate_parameters()

    def _calculate_via_dimensions(self, vias_size, vias_annular_width):
        if not vias_size:
            if self._minimum_vias_size > self._track_width:
                self._vias_size = self._minimum_vias_size
            else:
                self._vias_size = self._track_width
        else:
            if vias_size < self._minimum_vias_size:
                # TODO: Issue a warning!
                self._vias_size = self._minimum_vias_size
            else:
                self._vias_size = vias_size
        self._vias_inset = max(self._vias_size, self._track_width) / 2.0
        self._vias_separation = (
            max(self._vias_size, self._track_width) + self._track_separation
        )

        if not vias_annular_width:
            self._vias_annular_width = self._minimum_vias_annular_width
        else:
            if vias_annular_width < self._minimum_vias_annular_width:
                # TODO: Issue a warning!
                self._vias_annular_width = self._minimum_vias_annular_width
            else:
                self._vias_annular_width = vias_annular_width

    def _calculate_inner_vias_positions(self):
        self._inner_via_count = self._inbound_layer_count + 1
        self._inner_via_distance = float(
            (self._inner_via_count - 1) * self._vias_separation
        )

        inner_vias_intersection = closestIntersectionPoint(
            np.array(self._annotation.origin),
            self._annotation.direction,
            self._fronts[-1],
            self._EXTENSION,
        )
        # move by half the track width in the same direction and create a point there
        inner_vias_reference = Point(
            np.around(
                inner_vias_intersection.coords
                + self._annotation.direction * from_meter(self._vias_inset)
            ).flatten()
        )
        # extend from there perpendicularly by (inner_via_count - 1)*(track_width + track_separation)/2.0 in both directions and create two points
        perpendicular = normalize(makePerpendicular(self._annotation.direction))
        inner_vias_start = Point(
            inner_vias_reference.coords[0]
            + perpendicular * from_meter(-self._inner_via_distance) / 2.0
        )
        # place inner_via_count vias between those points
        self._inner_via_positions = []
        for i in range(self._inner_via_count):
            self._inner_via_positions.append(
                Point(
                    inner_vias_start.coords[0]
                    + i * perpendicular * from_meter(self._vias_separation)
                )
            )

    def _calculate_outer_vias_positions(self):
        self._outer_via_count = self._outbound_layer_count

        self._outer_via_distance = float(
            (self._outer_via_count - 1) * self._vias_separation
        )

        vias_intersection = closestIntersectionPoint(
            np.array(self._annotation.origin),
            self._annotation.direction,
            self._milled_contour,
            self._EXTENSION,
        )
        # move by the larger of vias_size/2.0 or track_width/2.0 in direction from the outer intersection point
        vias_reference = Point(
            np.around(
                vias_intersection.coords[0]
                + self._annotation.direction * from_meter(self._vias_inset)
            ).flatten()
        )
        # extend from there perpendicularly by (outer_via_count - 1)*(track_width + track_separation)/2.0
        perpendicular = normalize(makePerpendicular(self._annotation.direction))
        vias_end = Point(
            vias_reference.coords[0]
            + perpendicular * from_meter(self._outer_via_distance) / 2.0
        )
        vias_start = Point(
            vias_reference.coords[0]
            + perpendicular * from_meter(-self._outer_via_distance) / 2.0
        )
        # place outer_via_count vias between those points
        self._outer_via_positions = []
        for i in range(self._outer_via_count):
            self._outer_via_positions.append(
                Point(
                    vias_start.coords[0]
                    + i * perpendicular * from_meter(self._vias_separation)
                )
            )
        # create a shapely line segment between those two points and buffer it by via_size/2.0
        polygon = LineString([vias_end, vias_start]).buffer(
            from_meter(self._vias_inset + self._track_separation)
        )
        # use the buffered polygon in _create_track() to substract from the mill edge before creating the tracks
        self._start_contour = self._milled_contour - polygon

    def _create_milled_contour(self):
        """Create the milled contour of the board by buffering with the edge separation"""
        self._milled_contour = clear_multi_polygon(
            self._substrate.substrates.buffer(from_meter(-self._edge_clearance))
            .buffer(from_meter(-self._smallest_gap / 2.0))
            .buffer(from_meter(self._smallest_gap / 2.0))
            .simplify(self._SIMPLIFY)
        )

    def _create_track(self):
        self._calculate_outer_vias_positions()

        # create a list from the extracted interior rings
        interiors = list(clear_multi_polygon(self._start_contour).interiors)

        # prepare lists to store the fronts and the tracks
        fronts = [clear_multi_polygon(self._start_contour).exterior]
        tracks = []
        lengths = []
        areas = []

        for i in range(self._winding_count):
            expendables, interiors = separate_expendables(
                fronts[i],
                interiors,
                from_meter(self._smallest_gap),
            )

            # Find next front from last front in inwards direction
            fronts.append(
                clear_multi_polygon(
                    Polygon(fronts[i], expendables)
                    .buffer(from_meter(-(self._track_width + self._track_separation)))
                    .simplify(self._SIMPLIFY)
                ).exterior
            )

            # Find track from new front in outwards direction
            track = (
                fronts[i + 1]
                .buffer(from_meter(self._track_width / 2.0 + self._track_separation))
                .simplify(self._SIMPLIFY)
                .exterior
            )
            if track.is_empty:
                TopologicalError("Track is empty.")

            tracks.append(track)

            # Calculate and store the minimum separation between tracks
            if len(tracks) > 1:
                min_dist = track.distance(tracks[-2]) / 1e9
                min_separation = min_dist - self.track_width
                if (
                    self._min_track_separation is None
                    or min_separation < self._min_track_separation
                ):
                    self._min_track_separation = min_separation

            lengths.append(track.length / 1e9)

            areas.append(Polygon(track).area / 1e18)

        self._fronts = fronts
        self._tracks = tracks
        self._lengths = lengths
        self._areas = areas
        self._interiors = interiors

        # offset the innermost track by the track width plus the track separation
        expendables, interiors = separate_expendables(
            self._fronts[-1],
            self._interiors,
            from_meter(self._smallest_gap),
        )

        final_frontier = clear_multi_polygon(
            Polygon(self._fronts[-1], expendables)
            .buffer(from_meter(-(self._track_width + self._track_separation)))
            .simplify(self._SIMPLIFY)
        ).exterior
        self._fronts.append(final_frontier)

        final_track = (
            final_frontier.buffer(
                from_meter(self._track_width / 2.0 + self._track_separation)
            )
            .simplify(self._SIMPLIFY)
            .exterior
        )
        if final_track.is_empty:
            TopologicalError("Final track is empty.")
        self._final_track = final_track

        # add inner vias
        self._calculate_inner_vias_positions()

    def _calculate_conductor_length(self):
        self._conductor_length = self._layer_count * sum(self._lengths)

    def _calculate_cross_sectional_area(self):
        self._cross_sectional_area = self._track_width * self._copper_thickness

    def _calculate_enclosed_area(self):
        self._enclosed_area = self._layer_count * sum(self._areas)

    def _calculate_winding_width(self):
        self._winding_width = (
            self._winding_count * (self._track_width + self._track_separation)
            - self._track_separation
        )

    def _extract_annotation(self, board: pcbnew.BOARD) -> None:
        """Extract the TabAnnotation that marks the position of the vias used for switching layers"""
        annotations = []
        annotation_reader: AnnotationReader = AnnotationReader.getDefault()

        for footprint in board.GetFootprints():
            if annotation_reader.isAnnotation(footprint):
                annotations.extend(annotation_reader.convertToAnnotation(footprint))
        if len(annotations) < 1:
            ValueError("No annotation found, can not create coil geometry.")
        elif len(annotations) > 1:
            UserWarning(
                "More than one tab annotation found, using first detected annotation to place vias"
            )
        annotation: TabAnnotation = annotations[0]
        annotation.origin = (annotation.origin.x, annotation.origin.y)
        annotation.direction = normalize(annotation.direction)
        self._annotation = annotation

    def _extract_substrate(self, board: pcbnew.BOARD) -> None:
        self._substrate = extract_substrate(board)

    def _set_layer_count(self, layer_count: int):
        self._layer_count = int(layer_count)
        self._outbound_layer_count = int(np.ceil(layer_count / 2))
        self._inbound_layer_count = int(layer_count - self._outbound_layer_count)

    def __str__(self) -> str:
        return (
            f"edge clearance:          {self._edge_clearance * 1e3:.3f} mm\n"
            f"track width:             {self._track_width * 1e3:.3f} mm\n"
            f"nom. track separation:   {self._track_separation * 1e3:.3f} mm\n"
            f"min track separation:    {self._min_track_separation * 1e3:.3f} mm\n"
            f"vias size:               {self._vias_size * 1e3:.3f} mm\n"
            f"vias annular width:      {self._vias_annular_width * 1e3:.3f} mm\n"
            f"copper thickness:       {self._copper_thickness * 1e6:.3f} µm\n"
            f"winding count:          {self._winding_count:2d}\n"
            f"layer count:            {self._layer_count:2d}\n"
            f"voltage:                 {self._voltage:.3f} V\n"
            f"temperature:           {self._temperature:5.1f}°C\n"
            f"enclosed area:           {self._enclosed_area:.3f} m²\n"
            f"track length:           {self._conductor_length:.3f} m\n"
            f"cross-section:           {self._cross_sectional_area*1e6:.3f} mm²\n"
            f"resistance:             {self._resistance:.3f} Ohm\n"
            f"current:                 {self._current:.3f} A\n"
            f"magnetic dipole:         {self._magnetic_dipole:.3f} Am²\n"
            f"power:                   {self._power:.3f} W\n"
            f"dipole-power-ratio:      {self._dipole_power_ratio:.3f}\n"
        )

    def update(self):
        self._create_milled_contour()
        self._create_track()
        self._calculate_parameters()

    def update_parameters(self):
        self._calculate_parameters()

    def to_file(self, filename: str = None):
        """Print the text returned by __str__() to a file

        If no `filename` is provided, the log file is created at `<current-director>/coil.log`.
        """
        if not filename:
            filename = os.path.join(os.getcwd(), "coil.log")

        with open(filename, "w") as log_file:
            log_file.write(self.__str__())

    @property
    def annotation(self):
        return self._annotation

    @property
    def final_track(self):
        return self._final_track

    @property
    def inner_via_positions(self):
        return self._inner_via_positions

    @property
    def outer_via_positions(self):
        return self._outer_via_positions

    @property
    def milled_contour(self):
        """Polygon representing the milled contour of the board"""
        return self._milled_contour

    @property
    def tracks(self):
        return self._tracks

    @property
    def fronts(self):
        return self._fronts

    @property
    def edge_clearance(self):
        return self._edge_clearance

    @edge_clearance.setter
    def edge_clearance(self, value):
        self._edge_clearance = value
        if not self._explicit_update:
            self._create_milled_contour()
            self._create_track()
            self._calculate_parameters()

    @property
    def winding_count(self):
        return self._winding_count

    @winding_count.setter
    def winding_count(self, value):
        self._winding_count = value
        if not self._explicit_update:
            self._create_track()
            self._calculate_parameters()

    @property
    def track_width(self):
        return self._track_width

    @track_width.setter
    def track_width(self, value):
        self._track_width = value
        # self._track_distance = self._track_width + self._track_separation
        if not self._explicit_update:
            self._create_track()
            self._calculate_parameters()

    @property
    def track_separation(self):
        return self._track_separation

    @track_separation.setter
    def track_separation(self, value):
        self._track_separation = value
        # self._track_distance = self._track_width + self._track_separation
        if not self._explicit_update:
            self._create_track()
            self._calculate_parameters()

    @property
    def layer_count(self):
        return self._layer_count

    @layer_count.setter
    def layer_count(self, value):
        self._set_layer_count(value)
        if not self._explicit_update:
            self._calculate_parameters()

    @property
    def outbound_layer_count(self):
        return self._outbound_layer_count

    @property
    def inbound_layer_count(self):
        return self._inbound_layer_count

    @property
    def copper_thickness(self):
        return self._copper_thickness

    @copper_thickness.setter
    def copper_thickness(self, value):
        self._copper_thickness = value
        if not self._explicit_update:
            self._calculate_parameters()

    @property
    def winding_width(self):
        return self._winding_width

    @property
    def vias_size(self):
        return self._vias_size

    @property
    def vias_anular_width(self):
        return self._vias_annular_width

    @vias_anular_width.setter
    def vias_anular_width(self, value):
        self._vias_annular_width = value
        if not self._explicit_update:
            self._calculate_parameters()

    @property
    def properties(self):
        properties_dictionary = super().properties

        properties_dictionary["copper_thickness"] = self._copper_thickness
        properties_dictionary["layer_count"] = self._layer_count
        properties_dictionary["edge_clearance"] = self._edge_clearance
        properties_dictionary["track_separation"] = self._track_separation
        properties_dictionary["track_width"] = self._track_width
        properties_dictionary["winding_count"] = self._winding_count
        properties_dictionary["vias_size"] = self._vias_size
        properties_dictionary["vias_annular_width"] = self._vias_annular_width

        return properties_dictionary
