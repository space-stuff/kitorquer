# KiTorquer

KiTorquer is a KiCAD plugin for the automated generation of embedded PCB coils to be used as CubeSat magnetic actuators.

## Installation

KiTorquer requires [KiKit](https://github.com/yaqwsx/KiKit) which you will have to install first.
Please note that KiKit is a two-step installation:
1. Install KiKit via `pip`;
2. Install the KiKit Pcbnew action plugins via KiCAD's PCM.

Please see KiKit's detailled [installation guide](https://yaqwsx.github.io/KiKit/latest/installation/intro/).

Then clone the KiTorquer git repository to the KiCAD plugin directory:
```
$ cd ~/.local/share/kicad/7.0/scripting/plugins
$ git clone git@gitlab.com:space-stuff/kitorquer.git
```
Alternatively, download the zipped repo and extract it to the KiCAD plugin directory.
