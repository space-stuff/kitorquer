# -*- coding: utf-8 -*-

import logging

logging.basicConfig(filename="kitorquer.log", encoding="utf-8", level=logging.INFO)

from . import kitorquer
from .kitorquer.coil_drawer import CoilDrawer

import numpy as np
import os
from pcbnewTransition import pcbnew


class MagnetorquerPlugin(pcbnew.ActionPlugin):
    """Plugin to create a spacecraft magnetorquer as an embedded air coil."""

    def defaults(self):
        self.name = "KiTorquer"
        self.category = "Create"
        self.description = "Create a magnetorquer layout"
        self.icon_file_name = os.path.join(
            os.path.dirname(__file__), "converter.svg.png"
        )

    def Run(self):
        # TODO: [FEATURE] Buffer more than the track width to the inside to further smooth the generated coil design?
        #      Then the buffering to the outside would need to be done with that additional distance plus half the track width.

        layer_count = 8
        winding_count = 13
        track_width = 335e-6
        track_separation = 105e-6
        copper_thickness = 35e-6

        temperature = 20
        voltage = 3.3

        netcode = 0  # TODO: set this via the dialog

        board: pcbnew.BOARD = pcbnew.GetBoard()

        nets: pcbnew.NETNAMES_MAP = board.GetNetsByName()
        print(list(set([net.__str__() for net in nets])))
        print(board.GetNetsByNetcode())

        coil = kitorquer.EmbeddedAirCoil(
            board,
            winding_count,
            track_width,
            track_separation,
            layer_count,
            copper_thickness,
            temperature,
            voltage,
            vias_size=560e-6,
            vias_annular_width=130e-6,
            explicit_update=True,
        )

        # To optimize, set the following flag to True and provide ranges for winding count, track width, and voltage.
        optimize = True
        if optimize:
            minimum_dipole = 0.024  # Am²
            maximum_power = 0.8  # W
            coil_properties = []
            for winding_count in range(5, 16, 1):
                for track_width in np.arange(1000e-6, 3001e-6, 100e-6):
                    winding_width = (
                        winding_count * (track_width + track_separation)
                        - track_separation
                    )
                    if winding_width > 40.0e-3:
                        # TODO: Get this value automatically, seems to coincide with b - 2*d
                        # NOTE: Requires a little bit more work:
                        #       1. Find the largest distances in x and y that could be used for the coil - i.e. take care of offsets and so on.
                        #       2. Take the smaller of the two distances and compare its half by the winding width.
                        #       3. Create a proper logger entry if the limit is exceeded.
                        print("Break because winding width exceeds limit")
                        break
                    try:
                        coil.winding_count = winding_count
                        coil.track_width = float(track_width)
                        coil.update()
                    except:
                        print("Continue, because resulting geometry is invalid")
                        continue
                    # for v in np.arange(3.3, 3.3, 0.1):
                    for voltage in [1.8, 2.0, 2.5, 3.3]:
                        coil.voltage = voltage
                        coil.update_parameters()
                        # TODO: Allow for a more general cost function for optimization
                        if (
                            coil.magnetic_dipole >= minimum_dipole
                            and coil.power <= maximum_power
                        ):
                            coil_properties.append(coil.properties)
            dipoles = [c["magnetic_dipole"] for c in coil_properties]
            powers = [c["power"] for c in coil_properties]

            optimum_index = dipoles.index(max(dipoles))
            # optimum_index = powers.index(min(powers))
            optimum_coil_properties = coil_properties[optimum_index]

            # TODO: have a second constructor that allows to construct the coil from the properties object.
            coil = kitorquer.EmbeddedAirCoil(
                board,
                optimum_coil_properties["winding_count"],
                optimum_coil_properties["track_width"],
                optimum_coil_properties["track_separation"],
                optimum_coil_properties["layer_count"],
                optimum_coil_properties["copper_thickness"],
                optimum_coil_properties["temperature"],
                optimum_coil_properties["voltage"],
                optimum_coil_properties["vias_size"],
                optimum_coil_properties["vias_annular_width"],
                explicit_update=False,
            )
        print(coil)

        coil_drawer = CoilDrawer(board, coil)
        coil_drawer.draw(netcode, debug=False)

        coil.to_file()

        pcbnew.Refresh()


logging.info("Registering KiTorquer as a plugin")
MagnetorquerPlugin().register()
logging.info("Success")
